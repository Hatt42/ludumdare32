var LudumGame = LudumGame || {};

//LudumGame.game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, '');
LudumGame.game = new Phaser.Game(800, 600, Phaser.AUTO, '');

LudumGame.game.state.add('Boot', LudumGame.Boot);
//uncomment these as we create them through the tutorial
LudumGame.game.state.add('Preload', LudumGame.Preload);
LudumGame.game.state.add('MainMenu', LudumGame.MainMenu);
LudumGame.game.state.add('Game', LudumGame.Game);

LudumGame.game.state.start('Boot');
