var LudumGame = LudumGame || {};

//loading the game assets
LudumGame.Preload = function(){};

LudumGame.Preload.prototype = {
    preload: function() {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloadbar');
        this.preloadBar.anchor.setTo(0.5);

        this.load.setPreloadSprite(this.preloadBar);

        //load game assets
        this.load.spritesheet('player', 'assets/images/player.png', 32, 64, 5);
        this.load.spritesheet('civilian', 'assets/images/civilian.png', 32, 43, 2);
        this.load.image('tile', 'assets/images/tile.png');
        this.load.image('gas', 'assets/images/gas.png');
        

        //this.load.image('space', 'assets/images/space.png');
        //this.load.image('rock', 'assets/images/rock.png');
        //this.load.spritesheet('playership', 'assets/images/player.png', 12, 12);
        //this.load.spritesheet('power', 'assets/images/power.png', 12, 12);
        //this.load.image('playerParticle', 'assets/images/player-particle.png');
        //this.load.audio('collect', 'assets/audio/collect.ogg');
        //this.load.audio('explosion', 'assets/audio/explosion.ogg');
    },
    create: function() {
        //this.state.start('MainMenu');
        this.state.start('Game');
    }
};
