var LudumGame = LudumGame || {};

//title screen
LudumGame.Game = function() {};

var jumpTimer = 0;
var maxSpeed = 400;
var bumpSlow = 3;
var gasTimer = 0;

LudumGame.Game.prototype = {
	create: function() {
		// Set world dimensions
		this.game.world.setBounds(0,0,128000,400);

        this.game.stage.backgroundColor = '#fff';

        this.game.physics.arcade.gravity.y = 1200;

        maxSpeed = 400;
        bumpSlow = 3;

        this.tileNumber = 27;
        this.changeTimer = 0;
        this.changeInterval = 1000;
        this.level = 1;

		// Create player
		this.player = this.game.add.sprite(50, this.game.world.centerY - 150, 'player');
		//this.player.scale.setTo(1);
		//this.player.animations.add('fly', [0, 1, 2, 3], 5, true);
        this.player.animations.add('run');
		this.player.animations.play('run', 10, true);

		// Player initial score to zero
		this.playerScore = 0;
        this.scoreTimer = 0;

		// Enable player physics
		this.game.physics.arcade.enable(this.player);
        this.player.body.bounce.y = 0;
        // Move player
        this.player.body.velocity.x = 200;
        this.player.body.setSize(32,64,0,0);
		//this.playerSpeed = 100120;
		this.player.body.collideWorldBounds = true;
		
		// The camera will follow the player in the world
		this.game.camera.follow(this.player);
        
        // Create Ground
        this.generateGround();

        // Create Gas emitter
        this.createEmitter();

        // Civilians
        this.civilians = this.game.add.group();
        this.civilians.enableBody = true;
		this.civilians.physicsBodyType = Phaser.Physics.ARCADE;

        this.obstacleTimer = 0;

		// Sounds
		//this.collectSound = this.game.add.audio('collect');

		//show score
		this.showLabels();
        
        // Controlls keyboard and mouse
        this.jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.jumpButton.onDown.add(this.jump, this);

        this.input.onDown.add(this.jump, this);
	},
	update: function() {
        // Collision between player and ground
        this.game.physics.arcade.collide(this.player, this.tiles);
        this.game.physics.arcade.collide(this.emitter, this.tiles);
        this.game.physics.arcade.collide(this.civilians, this.tiles);
        this.game.physics.arcade.overlap(this.civilians, this.player, this.bumpInto, null, this);
        this.game.physics.arcade.collide(this.emitter, this.player, this.gameOver, null, this);

        //if (this.player.body.velocity.x == 0)
        //{
        //    this.game.time.events.add(800, this.gameOver, this);
        //}
        //console.log(this.player.body.velocity.y);
 
	    //update score
        if (this.game.time.now > this.scoreTimer)
        {
            this.playerScore++;
            this.scoreLabel.text = this.playerScore;
            this.scoreTimer = this.game.time.now + 100;
        }

        this.emitter.x += bumpSlow;
        if (this.game.time.now > gasTimer)
        {
            if (this.player.body.velocity.x < maxSpeed) {
                this.player.body.velocity.x += 20;
            }
            bumpSlow += 0.3;
            gasTimer = this.game.time.now + 5000;
        }
        //this.emitter.setXSpeed(this.player.body.velocity.x, this.player.body.velocity.x + 100);
    
        this.regenerateBlocks();
        this.createObstacle();
		// Collision between player and asteroids
		//this.game.physics.arcade.collide(this.player, this.asteroids, this.hitAsteroid, null, this);
	},
    bumpInto: function(a, b) {
        this.player.body.velocity.x -= 1;
    },
    generateGround: function() {
        this.tiles = this.game.add.group();
        this.tiles.enableBody = true;
		this.tiles.physicsBodyType = Phaser.Physics.ARCADE;

        var numTiles = 27;
        level = 1;
        var changeLvl;
        var tile;

		for (var i = 0; i <= numTiles; i++) {
            changeLvl = Math.floor(Math.random() * 5 + 1);
            if (changeLvl == 2)
            {
                //level = Math.floor(Math.random() * 3 + 1);
            }
			// Add sprite
			tile = this.tiles.create(i*32, this.game.world.centerY + level * 32, 'tile');

			// Physics properties
            tile.body.allowGravity = false;
			tile.body.immovable = true;
		}
    },
    regenerateBlocks: function() {
        this.tiles.forEach(function(tile) {
            if (tile.x + tile.width < this.camera.x) 
            {
                tile.destroy();
                this.addTile();
            }
        },this);
    },
    addTile: function() {
        if (this.game.time.now > this.changeTimer)
        {
            //this.level = Math.floor(Math.random() * 4 + 1);
            this.level = this.game.rnd.between(1, 2);
            this.changeTimer = this.game.time.now + this.changeInterval;
            //this.changeInterval -= 10;
        }
        // Add sprite
        this.tileNumber++;
        var tile = this.tiles.create(this.tileNumber * 32, this.game.world.centerY + this.level * 32 , 'tile');

        // Physics properties
        tile.body.allowGravity = false;
        tile.body.immovable = true;
    },
    createObstacle: function() {
        if (this.game.time.now > this.obstacleTimer)
        {
            // Create civilians
            this.civilian = this.civilians.create(this.game.camera.x + this.game.camera.width + 64, this.game.world.centerY -32, 'civilian');
            this.civilian.animations.add('vomit');
            this.civilian.animations.play('vomit', 4, true);
            //this.game.physics.arcade.enable(this.civilian);
            //this.civilian.body.bounce.y = 0;

            this.obstacleTimer = this.game.time.now + this.game.rnd.between(2000, 5000);
        }
    },
    createEmitter: function() {
        this.emitter = this.game.add.emitter(0, this.game.world.centerY, 200);
        this.emitter.makeParticles('gas');

        this.emitter.setRotation(0,0);
        this.emitter.setAlpha(0.3, 0.8);
        this.emitter.setScale(0.5, 1);
        this.emitter.gravity = 200;
        this.emitter.bounce.setTo(0.1, 0.7);
        this.emitter.setXSpeed(0,100);
		this.game.physics.arcade.enable(this.emitter);

    
        //this.emitter.fixedToCamera = true;
        this.emitter.start(false, 1000, 10);
    },
    jump: function() {
         //&& this.game.time.now > jumpTimer
        if (this.player.body.velocity.y == 0) 
        {
            this.player.body.velocity.y = -400;
            jumpTimer = this.game.time.now + 0;
        }
       
    },
	showLabels: function() {
		// Score text
		var text = "0";
        var style = { font: "20px Arial", fill: "#000", align: "center" };
        this.scoreLabel = this.game.add.text( 50,  50, text, style);
        this.scoreLabel.fixedToCamera = true;
	},
	gameOver: function() {
		//pass it the score as a parameter
		this.game.state.start('MainMenu', true, false, this.playerScore);
	}
};
