LudumGame.MainMenu = function(){};

LudumGame.MainMenu.prototype = {
    create: function() {
        this.game.stage.backgroundColor = '#fff';

        //start game text
        var text = "Tap to begin";
        var style = { font: "30px Arial", fill: "#000", align: "center" };
        var t = this.game.add.text(this.game.width/2, this.game.height/2, text, style);
        t.anchor.set(0.5);

        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
        this.jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        //highest score
        //text = "Highest score: "+this.highestScore;
        //style = { font: "15px Arial", fill: "#fff", align: "center" };
        //var h = this.game.add.text(this.game.width/2, this.game.height/2 + 50, text, style);
        //h.anchor.set(0.5);
    },
    update: function() {
        if(this.jumpButton.isDown || this.game.input.activePointer.justPressed()) {
          this.game.state.start('Game');
        }
    },
    init: function(score){
        var score = score || 0;
        this.highestScore = this.highestScore || 0;
        this.highestScore = Math.max(score, this.highestScore);
    }
};
